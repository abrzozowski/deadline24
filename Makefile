#############################################################################
# Copyright (C) 2015  Rafał Kobel <rafyco1@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Rafał Kobel <rafyco1@gmail.com>
# Project: 
# Description:
#############################################################################


CC              = gcc
CXX             = g++
#INCPATH         = -I../mingw491_32/mkspecs/win32-g++'
LINKER          = 
LFLAGS          = -Wl,-subsystem,console -mthreads
ED              = sed
DEL_FILE        = rm -f
DEL_DIR         = rm -rf
MOVE            = mv -f
CHK_DIR_EXISTS  = test -d
MKDIR           = mkdir -p

OBJECTS_FILE_PREFIX = framework/src
OBJECTS_FILES   = $(OBJECTS_FILE_PREFIX)/Algoritm.o \
                  $(OBJECTS_FILE_PREFIX)/Inputer.o \
                  $(OBJECTS_FILE_PREFIX)/InputerLine.o \
                  $(OBJECTS_FILE_PREFIX)/main.o \

OBJECTS_DIR     = bin/

OBJECTS_FILEA   = $(OBJECTS_FILES) $(ALGORITM_DIR)/algoritmA.o
OBJECTS_FILEB   = $(OBJECTS_FILES) $(ALGORITM_DIR)/algoritmB.o
OBJECTS_FILEC   = $(OBJECTS_FILES) $(ALGORITM_DIR)/algoritmC.o
OBJECTS_FILED   = $(OBJECTS_FILES) $(ALGORITM_DIR)/algoritmD.o

TARGETA         = $(OBJECTS_DIR)/progA
TARGETB         = $(OBJECTS_DIR)/progB
TARGETC         = $(OBJECTS_DIR)/progC
TARGETD         = $(OBJECTS_DIR)/progD

OUT_DIR         = outputs


.SUFFIXES: .cpp .cc .cxx .c

.cpp.o:
	$(CXX) -c $(CXXFLAGS) $(INCPATH) -o $@ $<



####### Build rules

all: build

build: $(TARGETA) $(TARGETB) $(TARGETC) $(TARGETD)

clean:
	$(DEL_FILE) *.o $(OBJECTS_DIR)/*.o $(OBJECTS_FILE_PREFIX)/*.o $(ALGORITM_DIR)/*.o
    
remove:
	$(DEL_DIR) $(OBJECTS_DIR)
    
$(OUT_DIR):
	$(MKDIR) $(OUT_DIR)

$(TARGETA): $(OBJECTS_FILEA)
	$(CXX) $(LFLAGS) -o $(TARGETA) $(OBJECTS_FILEA)
	
$(TARGETB): $(OBJECTS_FILEB)
	$(CXX) $(LFLAGS) -o $(TARGETB) $(OBJECTS_FILEB)
	
$(TARGETC): $(OBJECTS_FILEC)
	$(CXX) $(LFLAGS) -o $(TARGETC) $(OBJECTS_FILEC)
	
$(TARGETD): $(OBJECTS_FILED)
	$(CXX) $(LFLAGS) -o $(TARGETD) $(OBJECTS_FILED)
    
runA: $(TARGETA) $(OUT_DIR)
	$(TARGETA)
	
runB: $(TARGETB) $(OUT_DIR)
	$(TARGETB)
	
runC: $(TARGETC) $(OUT_DIR)
	$(TARGETC)
	
runD: $(TARGETD) $(OUT_DIR)
	$(TARGETD)
	
	
	