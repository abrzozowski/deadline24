/*
 *     Copyright (C) 2015  Rafa� Kobel <rafyco1@gmail.com>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#define NAMEA "exampleA"
#define COUNTA 1

#define NAMEB "exampleB"
#define COUNTB 1

#define NAMEC "exampleC"
#define COUNTC 1

#define NAMED "exampleD"
#define COUNTD 1

