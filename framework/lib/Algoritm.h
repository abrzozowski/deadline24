/*
 *     Copyright (C) 2015  Rafał Kobel <rafyco1@gmail.com>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ALGORITM_H
#define	ALGORITM_H

#include <string>

int getCount();
std::string getName();

class Algoritm {
public:
    Algoritm();
    void exe();
    virtual ~Algoritm();
private:
    int beg;
    int end;
};

#endif	/* ALGORITM_H */

