/*
 *     Copyright (C) 2015  Rafał Kobel <rafyco1@gmail.com>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef INPUTER_H
#define	INPUTER_H

#include "../lib/InputerLine.h"

#include <string>
#include <vector>

class Inputer {
public:
    Inputer(std::string file);
    InputerLine getLine(int);
    InputerLine getLine();
    virtual ~Inputer();
private:
    std::vector <std::string> lines;
};

#endif	/* INPUTER_H */

