#include <iostream>
#include <vector>

using data = std::uintmax_t;

int main() {
    int T;
    std::cin >> T;
    
    while(T--) {
        data N, M, K, P, res = 0, Kbit = 0;
        std::cin >> N >> M >> K >> P;
        std::vector<data> row(M, 0);
        std::vector< data > occ(M, 0);
        for(int i = 0; i < P; ++i){
            int x, e, y;
            std::cin >> x >> e >> y;
            x--;
            e--;
            y--;
            for(int j = x; j <= e; ++j) {
                row[y] |= (1 << j);   
            }
        }
        for(data i = 0; i < K; ++i) {
            Kbit |= (1 << i);
        }

        for(data i = 0; i < M - 2; ++i) {

            data r1 = row[i];
            data r2 = row[i + 1];
            data r3 = row[i + 2];
            data c = 0;
            
            while(r1 && r2 && r3) {
               
                if(!((r1 + 1) & Kbit) && !((r2 + 1) & Kbit) && !((r3 + 1) & Kbit)) {
                     for(int j = c; j < c + K; ++j) {
                         if(!(occ[i] & (1 << j))) {
                             occ[i] |= (1 << j);
                             ++res;
                         }
                         if(!(occ[i + 1] & (1 << j))) {
                             occ[i + 1] |= (1 << j);
                             ++res;
                         }
                         if(!(occ[i + 2] & (1 << j))) {
                             occ[i + 2] |= (1 << j);
                             ++res;
                         }
                     }
                }
                r1 = r1 >> 1;
                r2 = r2 >> 1;
                r3 = r3 >> 1;
                c++;
            }
        }

        
        std::cout << res << std::endl;
        
    }
    
    return 0;
}
