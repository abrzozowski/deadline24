/*
 *     Copyright (C) 2015  Rafał Kobel <rafyco1@gmail.com>
 * 
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdlib>
#include "../framework/framework.h"

#ifndef EXAMP
#define EXAMP A
#ifend

using namespace std;

int getCount() {
    return COUNTA;
}

string getName() {
    return NAMEA;
}

/**
 * Rozwiązanie problemu.
 *
 * @param[in] in   wejście algorytmu @ref Inputer 
 * @param[out] out działa jak istream, można do niego kierować odpowiedzi, które wyświetlą się w odpowiednich plikach
 **/
void algoritm(const *Inputer in, Outputer out) 
{
    cout << "Zadanie NAMEA" << endl;
    for (int i = 0; i < in.count(); i++) {
        InputerLine il = in.getLine(i);
        //out << il.getInteger(1) << endl;
        //cout << "Linia nr " << i << ": " << il.getInteger(1) << endl;
    }
}

